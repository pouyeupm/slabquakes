# SlabQuakes

## What is SlabQuakes for?

This simple project classifies earthquakes regarding their distance to a given slab.
Please go to the master branch to find the codes.

## What does SlabQuakes use?
It uses two input data:

>1- a slab geometry that you put its data inside **DATA/slab data** folder and introduce the name of the files in **Init_Names.py**. If you want to consider constant strike, or constant thickness for your slab, you can introduce them in **Init_Names.py**, instead of their files name.

>2- a catalog that you put its data in **DATA/earthquake data** folder and introduce its name in **Init_Names.py**.

And it generates a new folder inside **JOB** folder, including separate subcatalog files that include the grouped events.

##### It is essential to make sure of two things:
That the introduced catalog and introduced slab geometries are similar in:

* depth sign (positive or negative)
* longitudinal range (0 to 360 or -180 to 180)

depth values must be in km.

_Although SlabQuakes is designed to be functional using data with positive depths and longitude range -180 to 180, but it is only tested for negative depths and longitude range 0 to 360. If you find any problem when using positive depth or longitude range -180 to 180 please let me know._


## How does SlabQuakes classify your catalog?
The current version has two methods for classification, and you decide which one to use. Both methods use this object:
```sh
Calssify.get_events(catalog, geographical window, min magnitude)
```
The `geographical window` can be edited by you in **Init_Limits.py**
the `min magnitude` is the minimum magnitude you want to classify.

#####  method 1:
classifying events using only the slab interface and giving a `marginal distance` to the slab interface (positive value and in km)`

```sh
slab_interface_classify(slab, marginal distance)
```
So, you will have the following classes:
* events that are not located above the slab (there is no slab data below or above them)
* events above the interface
* events below the interface 
* events inside interface band
* events above the interface band
* events below the interface band

##### method 2:
classifying events using the slab interface, as above, and also the slab thickness:
```sh
slab_interface_classify(slab, marginal distance)
```
So, you will have the following classes:
* events that are not located above the slab (there is no slab data below or above them)
* events above the interface
* events below the interface 
* events inside interface band
* events above the interface band
* events inside the slab
* events below the slab 

##### Please look into the **README** inside **master** branch.
Those steps can also be find inside **script_for_classifying.py** inside **master** branch.

### GOOD LUCK


### Some samples of earthquake catalog calssification in Central America:
.... To be completed 
![Screenshot](1.png)
![Screenshot](2.png)
